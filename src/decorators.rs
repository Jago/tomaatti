use rust_decimal::Decimal;
use rust_decimal_macros::dec;

pub trait Decorator {
    fn decorate(&self, value: Decimal) -> Decimal;
}

pub struct VatDecorator {
    multiplier: Decimal,
}

impl VatDecorator {
    pub fn with_percentage(percentage: Decimal) -> VatDecorator {
        VatDecorator {
            multiplier: dec![1] + percentage / Decimal::from(100),
        }
    }
}

impl Decorator for VatDecorator {
    fn decorate(&self, value: Decimal) -> Decimal {
        value * self.multiplier
    }
}

pub struct KiloWattHourDecorator {}

impl KiloWattHourDecorator {
    pub fn new() -> KiloWattHourDecorator {
        KiloWattHourDecorator {}
    }
}

impl Decorator for KiloWattHourDecorator {
    fn decorate(&self, value: Decimal) -> Decimal {
        value / Decimal::from(1000)
    }
}

pub struct TaxDecorator {
    tax_amount: Decimal,
}

impl TaxDecorator {
    pub fn with_amount(tax_amount: Decimal) -> TaxDecorator {
        TaxDecorator {
            tax_amount
        }
    }
}

impl Decorator for TaxDecorator {
    fn decorate(&self, value: Decimal) -> Decimal {
        value + self.tax_amount
    }
}