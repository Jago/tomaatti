use rust_decimal::Decimal;
use serde::{Serialize, Deserialize};
use serde_xml_rs::from_str;


#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct DayAheadXml {
    #[serde(rename = "createdDateTime")]
    created_date_time: String,
    #[serde(rename = "period.timeInterval")]
    period_time_interval: TimeInterval,
    #[serde(rename = "TimeSeries")]
    pub time_series: Vec<TimeSeries>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct TimeInterval {
    pub start: String,
    pub end: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct TimeSeries {
    #[serde(rename = "Period")]
    pub period: Period,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct Period {
    #[serde(rename = "$value")]
    pub content: Vec<PeriodContent>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub enum PeriodContent {
    #[serde(rename = "timeInterval")]
    TimeInterval(TimeInterval),
    #[serde(rename = "resolution")]
    Resolution(String),
    Point(Point),
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct Point {
    pub position: usize,
    #[serde(rename = "price.amount", with = "rust_decimal::serde::str")]
    pub price: Decimal,
}

impl DayAheadXml {
    pub fn from(xml: &str) -> DayAheadXml {
        from_str(xml).unwrap()
    }
}

