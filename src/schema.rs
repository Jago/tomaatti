// @generated automatically by Diesel CLI.

diesel::table! {
    hourly_prices (hour) {
        hour -> Datetime,
        price -> Nullable<Decimal>,
    }
}
