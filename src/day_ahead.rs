use chrono::{DateTime, TimeZone, Local, Duration, Utc};
use rust_decimal::Decimal;

use crate::market_data::{DayAheadXml, PeriodContent::*};

#[derive(Debug)]
pub struct DayAhead {
    pub prices: Vec<HourlyPrice>,
}

#[derive(Debug)]
pub struct HourlyPrice {
    pub time: DateTime<Local>,
    pub price: Decimal,
}

impl From<&DayAheadXml> for DayAhead {
    fn from(dax: &DayAheadXml) -> DayAhead {
        let mut prices = vec![];
        for ts in dax.time_series.iter() {
            let mut time_interval = &crate::market_data::TimeInterval {
                start: "".to_string(),
                end: "".to_string(),
            };
            let mut resolution = "";
            let mut points = vec![];

            let period = &ts.period;

            for content in &period.content {
                match content {
                    TimeInterval(ti) => time_interval = ti,
                    Resolution(res) => resolution = res,
                    Point(point) => points.push(point),                
                }
            }

            println!("Datetime: {:#?}", time_interval.start);
            let start = Utc.datetime_from_str(&time_interval.start, "%Y-%m-%dT%H:%MZ").unwrap().with_timezone(&Local);

            let increment = match resolution {
                "PT15M" => 15,
                "PT30M" => 30,
                "PT60M" => 60,
                _ => 0
            };


            for point in points {
                let time = start + Duration::minutes(((point.position - 1) * increment).try_into().unwrap());
                let price = point.price;
                prices.push(HourlyPrice { time: time, price: price })
            }
        }

        DayAhead {
            prices: prices,
        }
    }
}