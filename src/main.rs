#[macro_use] extern crate rocket;
use rust_decimal_macros::dec;

use std::fs;

use decorators::{KiloWattHourDecorator, TaxDecorator, Decorator, VatDecorator};
use market_data::DayAheadXml;
use rust_decimal::Decimal;

use crate::day_ahead::DayAhead;

mod market_data;
mod day_ahead;
mod decorators;

#[get("/")]
fn index() -> &'static str {
    "Hello world!"
}

#[launch]
fn launch() -> _ {
    let decorators: Vec<Box<dyn Decorator>> = vec![
        Box::new(KiloWattHourDecorator::new()),
        //Box::new(TaxDecorator::with_amount(Decimal::from(dec!(0.02253)))),
        Box::new(VatDecorator::with_percentage(Decimal::from(10))),
    ];
    let xml = fs::read_to_string("test-data/2022-12-03-05.xml")
        .expect("Can't read the file");
    let dax = DayAheadXml::from(&xml);
    let da:DayAhead = DayAhead::from(&dax);

    for hourly_price in da.prices.iter() {
        let price = decorators.iter().fold(hourly_price.price, |acc, dec| dec.decorate(acc));
        let time = hourly_price.time;

        println!("{} : {}", time, price);
    }
    rocket::build().mount("/", routes![index])
}
