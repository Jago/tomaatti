CREATE TABLE hourly_prices (
    hour DATETIME PRIMARY KEY,
    price DECIMAL(10,2)
)
